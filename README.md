# README #

Movie Quiz app that generates movie-related questions and gives the user option to answer them.

### REQUIREMENTS: ###

The required python modules for this app is rdflib.

### USAGE: ###

Run the script and follow the prompts.
It will take some minutes to load the graph.
It also usually takes a few seconds to generate questions and alternatives.